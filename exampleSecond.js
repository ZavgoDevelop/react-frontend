// Sample provider for auth

import React from 'react';

import {
  AuthByPhoneService,
  AuthByEmailService,
  ApproveAuthByPhoneService,
  ApproveAuthByEmailService
} from '../../../common/api';

import LoginComponent from '../components/Login';
import { RequestProvider } from '../../../view/components/RequestContext';
import { AuthorisationProvider } from '../../../view/components/AuthorisationContext';

const Login = () => (
  <AuthorisationProvider>
    {({ login, token }) => (
      <RequestProvider
        withAuth
        requests={{
          authByPhone: AuthByPhoneService.getRequest,
          authByEmail: AuthByEmailService.getRequest,
          approveAuthByPhone: ApproveAuthByPhoneService.postRequest,
          approveAuthByEmail: ApproveAuthByEmailService.postRequest
        }}
      >
        {({ authByPhone, authByEmail, approveAuthByPhone, approveAuthByEmail }) => (
          <LoginComponent
            login={login}
            token={token}
            authByPhone={authByPhone}
            authByEmail={authByEmail}
            approveAuthByPhone={approveAuthByPhone}
            approveAuthByEmail={approveAuthByEmail}
          />
        )}
      </RequestProvider>
    )}
  </AuthorisationProvider>
);

export default Login;

