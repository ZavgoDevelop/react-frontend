// Sample Code Helper 


import React from 'react';
import PropTypes from 'prop-types';
import { UsersContext } from './usersContext';

class UsersProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      users: [],
      limit: 1000,
      isLoading: false,
      isLoadingUsers: false
    };
  }

  fetchUsers = async ({ venueId, limit, page }) => {
    this.setState({ isLoadingUsers: true });
    try {
      const getUsers = await GetVenueUsers.getRequest({ venueId, limit, page });
      this.setState({
        users: getUsers.users,
        isLoadingUsers: false
      });
    } catch {
      this.setState({ isLoadingUsers: false });
    }
  };

  addUser = async ({ role, name, email, phone, venueId, timeZone }) => {
    this.setState({ isLoading: true });
    try {
      const response = await AddUser.postRequest({
        role: role.toUpperCase(),
        name,
        email,
        phone,
        venueId,
        timeZone
      });
      return response.success;
    } catch {
      this.setState({ isLoading: false });
      return false;
    }
  };

  deleteUser = async ({ targetId, venueId }) => {
    await RemoveUser.postRequest({ targetId, venueId });
  };

  claimUserRole = async ({ userId, newRole, venueId }) => {
    const { page, limit } = this.state;

    try {
      await ClaimUserRole.postRequest({
        venueId,
        newRole: newRole.toUpperCase(),
      });

      await this.fetchUsers({ venueId, limit, page });
    } catch (e) {
      console.log(e);
    }
  };

  leaveVenue = async ({ venueId }) => {
    await LeaveVenue.postRequest({ venueId });
  }

  changeRole = async ({ targetUserId, newRole, oldRole, venueId }) => {
    await ChangeRole.postRequest({
      venueId,
      targetUserId,
      newRole: newRole.toUpperCase(),
      oldRole: oldRole.toUpperCase()
    });

    this.setState(({ users }) => ({
      users: users.map((user) => (user.id === targetUserId ? ({ ...user, role: newRole }) : user))
    }));
  };

  approveOwner = async ({ id, status, venueId }) => {
    try {
      await ApproveOwner.postRequest({ id, status, venueId });
      this.setState(({ users }) => ({
        users: users.map((user) => (user.id === id ? ({ ...user, claimStatus: status }) : user))
      }));
    } catch (e) {
      console.log(e);
    }
  };

  approveDeclineClaim = async ({ targetUserId, status, venueId }) => {
    const { page, limit } = this.state;
    
    try {
      await ApproveDeclineClaim.postRequest({
        status,
        venueId,
        targetUserId,
      });

      await this.fetchUsers({ venueId, limit, page });

      this.setState(({ users }) => ({
        users: users.map((user) => (user.id === targetUserId ? ({ ...user, claimStatus: status }) : user))
      }));

    } catch (e) {
      console.log(e);
    }
  }

  rejectOwner = async ({ id, status, venueId }) => {
    const { page, limit } = this.state;
    try {
      await ApproveOwner.postRequest({ id, status, venueId });
      await this.fetchUsers({ venueId, limit, page });
    } catch (e) {
      console.log(e);
    }
  };

  getProviderValues = () => {
    const {
      users,
      isLoading,
      isLoadingUsers
    } = this.state;
    return {
      users,
      isLoading,
      isLoadingUsers,
      addUser: this.addUser,
      leaveVenue: this.leaveVenue,
      changeRole: this.changeRole,
      fetchUsers: this.fetchUsers,
      deleteUser: this.deleteUser,
      rejectOwner: this.rejectOwner,
      approveOwner: this.approveOwner,
      claimUserRole: this.claimUserRole,
      approveDeclineClaim: this.approveDeclineClaim
    };
  };

  render() {
    const { children } = this.props;
    return (
      <UsersContext.Provider value={this.getProviderValues()}>
        {children}
      </UsersContext.Provider>
    );
  }
}

UsersProvider.propTypes = {
  children: PropTypes.object
};

export default UsersProvider;

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

const merchantUserRoles = {
  USER: "User",
  ADMIN: "Admin",
  OWNER: "Owner",
}

export const ENotificationTypes = {
  LEAVE: "LEAVE",
  UPDATE: "UPDATE",
  INVITATION: "INVITATION",
  CLAIM_USER: "CLAIM_USER",
  CLAIM_OWNER: "CLAIM_OWNER",
  CLAIM_ADMIN: "CLAIM_ADMIN",
  CHANGE_ROLE: "CHANGE_ROLE",
  DELETE_USER: "DELETE_USER",
  APPROVE_CLAIM: "APPROVE_CLAIM",
  DECLINE_CLAIM: "DECLINE_CLAIM",
  CHANGE_HIS_ROLE: "CHANGE_HIS_ROLE",
  APPROVE_INVITATION: "APPROVE_INVITATION",
  DECLINE_INVITATION: "DECLINE_INVITATION"
};

export const approveTypes = [
  ENotificationTypes.APPROVE_CLAIM,
  ENotificationTypes.APPROVE_INVITATION,
]

export const declineTypes = [
  ENotificationTypes.DECLINE_CLAIM,
  ENotificationTypes.DECLINE_INVITATION,
]

export const getNotifications = (notifications) => {
  const [
    claimNotifications,
    leaveNotifications,
    updateNotifications,
    deleteUserNotifications,
    changeRoleNotifications,
    invitationsNotifications,
    approveClaimNotifications,
    declineClaimNotifications,
    approveInvitationNotifications,
    declineInvitationNotifications
  ] = notifications.reduce((acc, item) => {
    const { type } = item;

    switch (type) {
      case ENotificationTypes.LEAVE: acc[1].push(item);
        break;
      case ENotificationTypes.UPDATE: acc[2].push(item);
        break;
      case ENotificationTypes.DELETE_USER: acc[3].push(item);
        break;
      case ENotificationTypes.CHANGE_ROLE: acc[4].push(item);
        break;
      case ENotificationTypes.CHANGE_HIS_ROLE: acc[4].push(item);
        break;
      case ENotificationTypes.INVITATION: acc[5].push(item);
        break;
      case ENotificationTypes.APPROVE_CLAIM: acc[6].push(item);
        break;
      case ENotificationTypes.DECLINE_CLAIM: acc[7].push(item);
        break;
      case ENotificationTypes.APPROVE_INVITATION: acc[8].push(item);
        break;
      case ENotificationTypes.DECLINE_INVITATION: acc[9].push(item);
        break;
      case ENotificationTypes.CLAIM_USER: acc[0].push({ ...item, role: merchantUserRoles.USER });
        break;
      case ENotificationTypes.CLAIM_ADMIN: acc[0].push({ ...item, role: merchantUserRoles.ADMIN });
        break;
      case ENotificationTypes.CLAIM_OWNER: acc[0].push({ ...item, role: merchantUserRoles.OWNER });
        break;
      default:
        break;
    }

    return acc;
  }, [[], [], [], [], [], [], [], [], [], []]);

  return {
    claimNotifications,
    leaveNotifications,
    updateNotifications,
    deleteUserNotifications,
    changeRoleNotifications,
    invitationsNotifications,
    approveClaimNotifications,
    declineClaimNotifications,
    approveInvitationNotifications,
    declineInvitationNotifications,
  }
};

